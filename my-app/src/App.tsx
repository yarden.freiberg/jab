import { createMuiTheme, CssBaseline, ThemeProvider } from '@material-ui/core';
import React from 'react';
import './App.scss';
import { LoginPage } from './pages/login-page/login-page';

function App() {
  const isDarkMode = true;
  const theme = createMuiTheme({
    palette: {
      type: isDarkMode ? 'dark' : 'light',
    },
  });

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <LoginPage></LoginPage>
    </ThemeProvider>
  );
}

export default App;
