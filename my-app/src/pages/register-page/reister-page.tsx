import { Box, Button, TextField } from '@material-ui/core';
import React from 'react'
import "./style.scss"

export function RegisterPage(props: any) {
  return (
    <Box className="boxRegister">
      <div className="header">
      Register to JAB
      </div>
      <form className="formRegistry" noValidate autoComplete="off">
        <div className="formDetail"><TextField label="Full name" /></div>
        <div className="formDetail"><TextField label="Phone number" /></div>
        <div className="formDetail"><TextField label="Email adress" /></div>
        <div className="formDetail"><TextField
          id="date"
          label="Birthday"
          type="date"
          defaultValue="2017-05-24"
          className=""
          InputLabelProps={{
            shrink: true,
          }}
        /></div>
        <div className="formDetail"><TextField type="password" label="Password" /></div>
        <div className="formDetail"><TextField type="password" label="Confirm Password" /></div>
        <Button className="RegisterBtn" variant="contained">Register</Button>
      </form>
    </Box>
  );
}