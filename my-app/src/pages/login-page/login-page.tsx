import { Box, Button, TextField } from '@material-ui/core';
import React from 'react'
import "./style.scss"

export function LoginPage(props: any) {
  return (
    <Box className="boxLogin">
      <div className="header">
      Login to JAB
      </div>
      <form className="formLogin" noValidate autoComplete="off">
        <div className="formDetail"><TextField label="Email adress" /></div>
        <div className="formDetail"><TextField type="password" label="Password" /></div>
        <Button className="LoginBtn" variant="contained">Login</Button>
      </form>
    </Box>
  );
}